package me.engo.zerotwo.commands;

import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Translate;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;

public class Channelcreate extends ListenerAdapter {
	
public static String alias = "cc";
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "channelcreate") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

			String language;
			File languages = new File("Database/Language/" + Context.getAuthor().getId());
			if (languages.exists()) {
				File[] languages_ = languages.listFiles();
				assert languages_ != null;
				language = languages_[0].getName();
			} else {
				language = "english_en";
			}

			try {
				File ban = new File("Database/Bans/" + Context.getAuthor().getId());
				if (ban.exists()) {
					EmbedBuilder em = new EmbedBuilder();
					final String ban_reason;

					File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
					List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
					ban_reason = radky_duvodu.get(0);

					em.setDescription("Hey " + Context.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
					em.setFooter(c.footer1, c.footer2);
					em.setColor(new Color(c.Color));
					Context.getChannel().sendMessage(em.build()).queue();
					return;
				}
			} catch (IOException e){
				e.printStackTrace();
			}

			if (messageSent.length < 3) {
				String text = Translate.getTranslate(language, "advanced_warnings", "template_channel");
				Context.getChannel().sendMessage(text + c.prefix + "channelcreate general text`").queue();
			} else if (messageSent.length > 3) {
				String text = Translate.getTranslate(language, "basic_warnings", "high_parameters");
				Context.getChannel().sendMessage(text).queue();
			} else {
				if (c.moderation) {
					if (Objects.requireNonNull(Context.getMember()).getPermissions(Context.getChannel()).contains(Permission.MANAGE_CHANNEL)) {
						if (messageSent[2].toLowerCase().equals("text") || messageSent[2].toLowerCase().equals("voice")) {
							if (messageSent[2].equals("text")) {
								Context.getGuild().createTextChannel(messageSent[1]).queue();
							} else {
								Context.getGuild().createVoiceChannel(messageSent[1]).queue();
							}
							String text = Translate.getTranslate(language, "logs", "channel_created");
							Context.getChannel().sendMessage(text + messageSent[1].toLowerCase() + "'.").queue();
						} else {
							String text1 = Translate.getTranslate(language, "advanced_warnings", "invalid_channel_type_1");
							String text2 = Translate.getTranslate(language, "advanced_warnings", "invalid_channel_type_2");
							Context.getChannel().sendMessage(text1 + messageSent[2] + text2).queue();
						}
					} else {
						String text = Translate.getTranslate(language, "basic_warnings", "permissions_false");
						Context.getChannel().sendMessage(text).queue();
					}
				} else {
					String text = Translate.getTranslate(language, "basic_warnings", "moderationmodule_false");
					Context.getChannel().sendMessage(text).queue();
				}
			}
		}
	}
}
