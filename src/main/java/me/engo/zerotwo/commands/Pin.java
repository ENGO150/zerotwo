package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;

public class Pin extends ListenerAdapter {
	
	public static String alias = "There's no aliases.";
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
		Config c = new Config();
		String[] messageSent = event.getMessage().getContentRaw().split(" ");
		if (event.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "pin")) {

			try {
				String language;
				File languages = new File("Database/Language/" + event.getAuthor().getId());
				if (languages.exists()) {
					File[] languages_ = languages.listFiles();
					assert languages_ != null;
					language = languages_[0].getName();
				} else {
					language = "english_en";
				}

				try {
					File ban = new File("Database/Bans/" + event.getAuthor().getId());
					if (ban.exists()) {
						EmbedBuilder em = new EmbedBuilder();
						final String ban_reason;

						File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
						List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
						ban_reason = radky_duvodu.get(0);

						em.setDescription("Hey " + event.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
						em.setFooter(c.footer1, c.footer2);
						em.setColor(new Color(c.Color));
						event.getChannel().sendMessage(em.build()).queue();
						return;
					}
				} catch (IOException e){
					e.printStackTrace();
				}

				if (messageSent.length < 2) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
					event.getChannel().sendMessage(text).queue();
				} else {
					if (c.moderation) {
						if (Objects.requireNonNull(event.getMember()).hasPermission(event.getChannel(), Permission.MESSAGE_MANAGE)) {

							int a = messageSent.length;

							String[] slova = new String[a];

							System.arraycopy(messageSent, 1, slova, 0, a - 1);

							StringBuilder done = new StringBuilder();

							for (int i = 0; i < slova.length - 1; i++) {
								done.append(" ").append(slova[i]);
							}

							done = new StringBuilder(done.substring(1));

							String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("done").getAsString();
							event.getChannel().sendMessage(done).queue((message -> message.pin().queue()));
							event.getChannel().sendMessage(text).queue();
						} else {
							String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("permissions_false").getAsString();
							event.getChannel().sendMessage(text).queue();
						}
					} else {
						String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("moderationmodule_false").getAsString();
						event.getChannel().sendMessage(text).queue();
					}
				}
			} catch (FileNotFoundException e){
				e.printStackTrace();
			}
		}
	}
}
