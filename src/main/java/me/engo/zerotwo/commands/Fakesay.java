package me.engo.zerotwo.commands;

import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Translate;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Fakesay extends ListenerAdapter {
	
	public static String alias = "fs";
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "fakesay") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

			String language;
			File languages = new File("Database/Language/" + Context.getAuthor().getId());
			if (languages.exists()) {
				File[] languages_ = languages.listFiles();
				assert languages_ != null;
				language = languages_[0].getName();
			} else {
				language = "english_en";
			}

			try {
				File ban = new File("Database/Bans/" + Context.getAuthor().getId());
				if (ban.exists()) {
					EmbedBuilder em = new EmbedBuilder();
					final String ban_reason;

					File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
					List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
					ban_reason = radky_duvodu.get(0);

					em.setDescription("Hey " + Context.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
					em.setFooter(c.footer1, c.footer2);
					em.setColor(new Color(c.Color));
					Context.getChannel().sendMessage(em.build()).queue();
					return;
				}
			} catch (IOException e){
				e.printStackTrace();
			}

			File f = new File("Database/Premium/" + Context.getAuthor().getId());

			if (f.exists()) {
				Context.getMessage().delete().queue();

				if (messageSent.length < 3) {
					String text = Translate.getTranslate(language, "basic_warnings", "low_parameters");
					Context.getChannel().sendMessage(text).queue();
				} else {
					Member m = Context.getMessage().getMentionedMembers().get(0);

					int a = messageSent.length;

					String[] slova = new String[a];

					System.arraycopy(messageSent, 1, slova, 0, a - 1);

					StringBuilder done = new StringBuilder();

					for (int i = 1; i < slova.length - 1; i++) {
						done.append(" ").append(slova[i]);
					}

					done = new StringBuilder(done.substring(1));

					EmbedBuilder em = new EmbedBuilder();
					em.setDescription(m.getAsMention() + " said:\n" + done);
					em.setColor(new Color(c.Color));
					em.setFooter(c.footer1, c.footer2);

					Context.getChannel().sendMessage(em.build()).queue();

					String text = Translate.getTranslate(language, "logs", "done");
					Context.getAuthor().openPrivateChannel().queue((channel) ->
							channel.sendMessage(text).queue());
				}
			} else {
				String text = Translate.getTranslate(language, "basic_warnings", "permissions_false");
				Context.getChannel().sendMessage(text).queue();
			}
		}
	}
}
