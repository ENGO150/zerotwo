package me.engo.zerotwo.commands;

import me.engo.zerotwo.Bot;
import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Translate;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

public class Blowjob extends ListenerAdapter {
	
	public static String alias = "oral";
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "blowjob") || messageSent[0].equalsIgnoreCase(c.prefix + "oral")) {

			String language;
			File languages = new File("Database/Language/" + Context.getAuthor().getId());
			if (languages.exists()) {
				File[] languages_ = languages.listFiles();
				assert languages_ != null;
				language = languages_[0].getName();
			} else {
				language = "english_en";
			}

			try {
				File ban = new File("Database/Bans/" + Context.getAuthor().getId());
				if (ban.exists()) {
					EmbedBuilder em = new EmbedBuilder();
					final String ban_reason;

					File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
					List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
					ban_reason = radky_duvodu.get(0);

					em.setDescription("Hey " + Context.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
					em.setFooter(c.footer1, c.footer2);
					em.setColor(new Color(c.Color));
					Context.getChannel().sendMessage(em.build()).queue();
					return;
				}
			} catch (IOException e){
				e.printStackTrace();
			}

			if (messageSent.length < 3) {

				if (c.nsfw) {
					if (Context.getChannel().isNSFW()) {

						Bot.api.hasVoted(Context.getAuthor().getId()).whenComplete((hasVoted, e) -> {
							if (!hasVoted){
								String text = Translate.getTranslate(language, "basic_warnings", "vote_false");
								Context.getChannel().sendMessage(text).queue();
							} else {
								Random rnd = new Random();
								int rndm = (1 + rnd.nextInt(4)) - 1;

								String[] zt = new String[4];
								zt[0] = "https://cdn.discordapp.com/attachments/609425123291693056/668930227187220525/20734828.gif";
								zt[1] = "https://cdn.discordapp.com/attachments/609425123291693056/668926830870528049/bb5a405f-6351-4d4d-a1f6-aa7fbc199b63.gif";
								zt[2] = "https://cdn.discordapp.com/attachments/553515884660916235/690421912681185290/9ebdc170bf90b163d539a652f6a5840f.png";
								zt[3] = "https://cdn.discordapp.com/attachments/553515884660916235/688255348745699358/sally-whitemane-hentai-86_01DFWP9Z1ECEAABBY0ZM5W1QC7.png";

								EmbedBuilder em = new EmbedBuilder();

								if (messageSent.length == 2) {
									User m = Context.getMessage().getMentionedUsers().get(0);

									if (m == null){
										String text = Translate.getTranslate(language, "basic_warnings", "low_parameters");
										Context.getChannel().sendMessage(text).queue();
										return;
									}

									em.setDescription( Context.getAuthor().getName() + " blowjobs " + m.getName());
								}

								em.setImage(zt[rndm]);
								em.setFooter(c.footer1, c.footer2);
								em.setColor(new Color(c.Color));

								Context.getChannel().sendMessage(em.build()).queue();
							}
						});
					} else {
						String text = Translate.getTranslate(language, "basic_warnings", "nsfwchannel_false");
						Context.getChannel().sendMessage(text).queue();
					}
				} else {
					String text = Translate.getTranslate(language, "basic_warnings", "nsfwmodule_false");
					Context.getChannel().sendMessage(text).queue();
				}

			} else {

				String text = Translate.getTranslate(language, "basic_warnings", "high_parameters");

				Context.getChannel().sendMessage(text).queue();

			}
		}
	}
}
