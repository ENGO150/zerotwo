package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Bot;
import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Music;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Moan extends ListenerAdapter {

    public static String alias = "There's no aliases.";

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Config c = new Config();
        String[] messageSent = event.getMessage().getContentRaw().split(" ");
        if (event.getAuthor().isBot()) return;

        if (messageSent[0].equalsIgnoreCase(c.prefix + "moan")) {

            try {
                String language;
                File languages = new File("Database/Language/" + event.getAuthor().getId());
                if (languages.exists()) {
                    File[] languages_ = languages.listFiles();
                    assert languages_ != null;
                    language = languages_[0].getName();
                } else {
                    language = "english_en";
                }

                try {
                    File ban = new File("Database/Bans/" + event.getAuthor().getId());
                    if (ban.exists()) {
                        EmbedBuilder em = new EmbedBuilder();
                        final String ban_reason;

                        File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
                        List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
                        ban_reason = radky_duvodu.get(0);

                        em.setDescription("Hey " + event.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
                        em.setFooter(c.footer1, c.footer2);
                        em.setColor(new Color(c.Color));
                        event.getChannel().sendMessage(em.build()).queue();
                        return;
                    }
                } catch (IOException e){
                    e.printStackTrace();
                }

                if (messageSent.length < 2) {

                    if (c.nsfw) {
                        if (event.getChannel().isNSFW()) {

                            Bot.api.hasVoted(event.getAuthor().getId()).whenComplete((hasVoted, e) -> {
                                if (!hasVoted) {
                                    try {
                                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("vote_false").getAsString();
                                        event.getChannel().sendMessage(text).queue();
                                    } catch (FileNotFoundException ex) {
                                        ex.printStackTrace();
                                    }
                                } else {
                                    if (Objects.requireNonNull(Objects.requireNonNull(event.getMember()).getVoiceState()).inVoiceChannel() && Objects.requireNonNull(Objects.requireNonNull(Objects.requireNonNull(event.getMember()).getVoiceState()).getChannel()).getMembers().size() < 2) {
                                        try {
                                            int i = new Random().nextInt(8);
                                            if (i < 1) {
                                                i++;
                                            }
                                            new Music().loadAndPlay(event.getChannel(), "https://raw.githubusercontent.com/ZeroTwobt/moan/master/moan" + i + ".mp3");
                                        } catch (FileNotFoundException ex) {
                                            ex.printStackTrace();
                                        }
                                    } else if (!Objects.requireNonNull(event.getMember().getVoiceState()).inVoiceChannel()){
                                        try {
                                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_false").getAsString();
                                            event.getChannel().sendMessage(text).queue();
                                        } catch (FileNotFoundException ex) {
                                            ex.printStackTrace();
                                        }
                                    } else {
                                        Member m = event.getGuild().getMember(event.getJDA().getSelfUser());
                                        if (Objects.requireNonNull(event.getMember().getVoiceState().getChannel()).getMembers().contains(m) && event.getMember().getVoiceState().getChannel().getMembers().size() == 2){
                                            try {
                                                int i = new Random().nextInt(8);
                                                if (i < 1) {
                                                    i++;
                                                }
                                                new Music().loadAndPlay(event.getChannel(), "https://raw.githubusercontent.com/ZeroTwobt/moan/master/moan" + i + ".mp3");
                                            } catch (FileNotFoundException ex) {
                                                ex.printStackTrace();
                                            }
                                            return;
                                        }
                                        try {
                                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("alone_false").getAsString();
                                            event.getChannel().sendMessage(text).queue();
                                        } catch (FileNotFoundException ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                }
                            });

                        } else {
                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("nsfwchannel_false").getAsString();
                            event.getChannel().sendMessage(text).queue();
                        }
                    } else {
                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("nsfwmodule_false").getAsString();
                        event.getChannel().sendMessage(text).queue();
                    }

                } else {
                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
                    event.getChannel().sendMessage(text).queue();
                }
            } catch (FileNotFoundException e){
                e.printStackTrace();
            }
        }
    }
}
